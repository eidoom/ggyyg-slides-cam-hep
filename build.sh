#!/usr/bin/env bash

mkdir -p tikz
latexmk -pdf -pdflatex="pdflatex --shell-escape %O %S" slides.tex
