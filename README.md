# [ggyyg-slides-cam-hep](https://gitlab.com/eidoom/ggyyg-slides-cam-hep)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7763067.svg)](https://doi.org/10.5281/zenodo.7763067)

[Live here](https://eidoom.gitlab.io/ggyyg-slides-cam-hep/slides.pdf)

Fork of [`gitlab:eidoom/ggyynj-slides`](https://gitlab.com/eidoom/ggyynj-slides).

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
